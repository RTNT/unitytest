using UnityEngine;

namespace Ratana.Ch9.Interaction
{
 [CreateAssetMenu(menuName = "GameDev3/Util/ScriptableObjectUtils")]
 public class ScriptableObjectUtils : ScriptableObject
 {
  public void Destroy(GameObject gameObject)
  {
    Object.Destroy(gameObject);
  }
 }
}
