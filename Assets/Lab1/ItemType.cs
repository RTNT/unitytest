using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ratana
{
    public enum ItemType {
        COIN,
        BIGCOIN ,
        POWERUP ,
        POWERDOWN ,
        DIAMOND,
        BIGDIAMOND,
    }
}