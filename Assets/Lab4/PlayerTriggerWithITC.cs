using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Ratana;
using UnityEngine;

namespace Ratana.Ch4
{
     public class PlayerTriggerWithITC : MonoBehaviour
     {
       private void OnTriggerEnter(Collider other)
       {
         //Get components from item object
         //Get the ItemTypeComponent component from the triggered object
         ItemTypeComponent itc = other.GetComponent <ItemTypeComponent >();
        
         //Get components from the player
         //Inventory
         var inventory = GetComponent <Inventory >();
         //SimpleHealthPointComponent
         var simpleHP = GetComponent <SimpleHealthPointComponent >();
        
         if (itc !=null)
         {
             switch (itc.Type)
             {
                 case ItemType.COIN:
                 inventory.AddItem("COIN",1);
                 break;
                 case ItemType.BIGCOIN:
                 inventory.AddItem("BIGCOIN",1);
                 break;
                 case ItemType.POWERUP:
                 if(simpleHP != null)
                     simpleHP.HealthPoint = simpleHP.HealthPoint + 10;
                 break;
                 case ItemType.POWERDOWN:
                 if(simpleHP != null)
                     simpleHP.HealthPoint = simpleHP.HealthPoint - 10;
                 break;
                 case ItemType.DIAMOND:
                     inventory.AddItem("DIAMOND",1);
                     break;
                 case ItemType.BIGDIAMOND:
                     inventory.AddItem("BIGDIAMOND",3);
                     break;
             }
         }
        
        
         Destroy(other.gameObject ,0);
       }
     }
}

