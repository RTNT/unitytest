using UnityEngine;

namespace Ratana.Ch8
{
    public class MyAnimatorControlScript : MonoBehaviour
    {
        protected Animator ch2AnimatorController;

        /*private static readonly int Punch = Animator.StringToHash("Punch");
        private static readonly int Dancing = Animator.StringToHash("Dancing");
        private static readonly int State = Animator.StringToHash("State");
        private static readonly int Turn = Animator.StringToHash("Turn");*/

        private static readonly int p1_int = Animator.StringToHash("p1_int");
        private static readonly int p2_int = Animator.StringToHash("p2_int");
        private static readonly int p3_bool = Animator.StringToHash("p3_bool");
        private static readonly int p4_bool = Animator.StringToHash("p4_bool");
        private static readonly int p5_Trigger = Animator.StringToHash("p5_Trigger");
        private static readonly int p6_Trigger = Animator.StringToHash("p6_Trigger");
        

        private void Start()
        {
            ch2AnimatorController = GetComponent<Animator>();
        }

        void Update()
        {
           /* if (Input.GetKeyDown(KeyCode.Space))
            {
                ch2AnimatorController.SetTrigger("Jump");
            }

            if (Input.GetKeyDown(KeyCode.Z))
            {
                ch2AnimatorController.SetTrigger(Punch);
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                ch2AnimatorController.SetBool(Dancing, true);
            }

            if (Input.GetKeyDown(KeyCode.C))
            {
                ch2AnimatorController.SetInteger(State, 2);
            }

            if (Input.GetKeyDown(KeyCode.V))
            {
                ch2AnimatorController.SetFloat(Turn, 0.3f);
            }*/

            if (Input.GetKeyDown(KeyCode.W))
            {
                ch2AnimatorController.SetInteger(p1_int, 0);
                ch2AnimatorController.SetInteger(p2_int, 0);
                ch2AnimatorController.SetBool(p4_bool, false);
                ch2AnimatorController.SetBool(p3_bool, true);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                ch2AnimatorController.SetInteger(p1_int, 0);
                ch2AnimatorController.SetInteger(p2_int, 0);
                ch2AnimatorController.SetBool(p3_bool, false);
                ch2AnimatorController.SetBool(p4_bool, true);
            }
            
            if (Input.GetKeyDown(KeyCode.A))
            {
                ch2AnimatorController.SetInteger(p2_int, 0);
                ch2AnimatorController.SetInteger(p1_int, 1);
                ch2AnimatorController.SetBool(p3_bool, false);
                ch2AnimatorController.SetBool(p4_bool, false);
                
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                ch2AnimatorController.SetInteger(p1_int, 0);
                ch2AnimatorController.SetInteger(p2_int, 1);
                ch2AnimatorController.SetBool(p3_bool, false);
                ch2AnimatorController.SetBool(p4_bool, false);
                
            }
            
            if (Input.GetKeyDown(KeyCode.R))
            {
                ch2AnimatorController.SetInteger(p1_int, 0);
                ch2AnimatorController.SetInteger(p2_int, 0);
                ch2AnimatorController.SetBool(p4_bool, false);
                ch2AnimatorController.SetBool(p3_bool, false);
            }
            
            if (Input.GetKeyDown(KeyCode.Z))
            {
                ch2AnimatorController.SetTrigger("p5_Trigger");
                
            }
            if (Input.GetKeyDown(KeyCode.X))
            {
                ch2AnimatorController.SetTrigger("p6_Trigger");
                
            }
        }
    }
}